from flask import Flask , request,jsonify
import tensorflow as tf
import numpy as np
from PIL import Image, UnidentifiedImageError
import base64
from io import BytesIO



classnames_status = ["Damaged", "Not Damaged"]
classnames_category = ["Mobile", "TV"]

app = Flask(__name__)
Model_Path = 'Model/modelproductstatus_v4.h5'
model_status = tf.keras.models.load_model(Model_Path)
Model_Path = 'Model/modelproductsCategory_v1.h5'
model_category = tf.keras.models.load_model(Model_Path)

@app.route('/', methods=['GET'])
def index():
    # Main page
    return 'welcome to the app'

@app.route('/predict', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':

       try:
           f = request.form['file']

           #f = request.files['file']

           f = f.split(',')[1]



           #decoding the  base64 images into file object


           im_bytes = base64.b64decode(f)
           f = BytesIO(im_bytes)


           image = np.array(
                Image.open(f).convert("RGB").resize((256, 256))
            )

       except UnidentifiedImageError:
           return jsonify(predictedclass='UnidentifiedImageError')

       else:
           img_array = tf.expand_dims(image, 0)
           predictions = model_status.predict(img_array)
           predicted_StatusClass = classnames_status[np.argmax(predictions[0])]
           confidence = round(100 * (np.max(predictions[0])), 2)
           confidence = str(confidence)+'%'
           predictions = model_category.predict(img_array)
           predicted_CategoryClass = classnames_category[np.argmax(predictions[0])]
           return jsonify(predictedclass = predicted_StatusClass ,confidence = confidence ,
                          predictedcategory = predicted_CategoryClass)

    return None


if __name__ == '__main__':
    app.run(debug=True, port=4000)

